package com.jy.friendcutapi.configure;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@OpenAPIDefinition(
        info = @Info(title = "손절 장인 App",
                description = "받기만 하는 친구를 손절하자.",
                version = "v1"))
@RequiredArgsConstructor
@Configuration
public class SwaggerConfig {

    @Bean
    public GroupedOpenApi loveLineApi() {
        String[] paths = {"/v1/**"};

        return GroupedOpenApi.builder()
                .group("손절장인 API v1")
                .pathsToMatch(paths)
                .build();
    }
}
