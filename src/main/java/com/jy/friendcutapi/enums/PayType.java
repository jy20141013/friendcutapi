package com.jy.friendcutapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PayType {
    GIVE("줌"),
    TAKE("받음");

    private final String payType;
}
