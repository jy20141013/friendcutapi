package com.jy.friendcutapi.controller;

import com.jy.friendcutapi.model.friend.FriendCreateRequest;
import com.jy.friendcutapi.model.friend.FriendIsCutChangeRequest;
import com.jy.friendcutapi.model.friend.FriendItem;
import com.jy.friendcutapi.repository.FriendRepository;
import com.jy.friendcutapi.service.FriendService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/friend")
public class FriendController {
    private final FriendService friendService;
    @PostMapping("/new")
    public String setFriend(@RequestBody FriendCreateRequest request) {
        friendService.setFriend(request);
        return "OK";
    }

    @GetMapping("/all")
    public List<FriendItem> getFriends() {
        return friendService.getFriends();
    }

    @PutMapping("/is-cut/friend-id/{friendId}")
    public String putFriend(@PathVariable long friendId, @RequestBody FriendIsCutChangeRequest request) {
        friendService.putFriend(friendId,request);
        return "OK";
    }

    @PutMapping("/is-cut-cancel/friend-id/{friendId}")
    public String putFriendCutCancel(@PathVariable long friendId) {
        friendService.putFriendCutCancel(friendId);
        return "OK";
    }
}
