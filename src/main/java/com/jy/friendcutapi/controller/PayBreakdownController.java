package com.jy.friendcutapi.controller;

import com.jy.friendcutapi.entity.Friend;
import com.jy.friendcutapi.model.payBreakdown.PayBreakdownCreateRequest;
import com.jy.friendcutapi.model.payBreakdown.PayBreakdownItem;
import com.jy.friendcutapi.model.payBreakdown.PayBreakdownWhatPayChangeRequest;
import com.jy.friendcutapi.service.FriendService;
import com.jy.friendcutapi.service.PayBreakdownService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/pay-breakdown")
public class PayBreakdownController {
    private final PayBreakdownService payBreakdownService;
    private final FriendService friendService;

    @PostMapping("/new/friend-id/{friendId}")
    public String setPayBreakdown(@PathVariable long friendId, @RequestBody PayBreakdownCreateRequest request) {
        Friend friend = friendService.getFriend(friendId);
        payBreakdownService.setPayBreakdown(friend, request);
        return "OK";
    }

    @GetMapping("/all")
    public List<PayBreakdownItem> getPayBreakdowns() {
        return payBreakdownService.getPayBreakdowns();
    }

    @PutMapping("/what-pay/pay-breakdown-id/{payBreakdownId}")
    public String putWhatPay(@PathVariable long payBreakdownId, @RequestBody PayBreakdownWhatPayChangeRequest request) {
        payBreakdownService.putWhatPay(payBreakdownId,request);

        return "OK";
    }

    @DeleteMapping("/delete-pay-breakdown/pay-breakdown-id/{payBreakdownId}")
    public String delPayBreakdown(@PathVariable long payBreakdownId) {
        payBreakdownService.delPayBreakdown(payBreakdownId);

        return "OK";
    }
}
