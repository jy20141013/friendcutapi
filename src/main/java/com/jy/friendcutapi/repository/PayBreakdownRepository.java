package com.jy.friendcutapi.repository;

import com.jy.friendcutapi.entity.PayBreakdown;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PayBreakdownRepository extends JpaRepository<PayBreakdown, Long> {
}
