package com.jy.friendcutapi.repository;

import com.jy.friendcutapi.entity.Friend;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FriendRepository extends JpaRepository<Friend, Long> {
}
