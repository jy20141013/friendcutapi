package com.jy.friendcutapi.service;

import com.jy.friendcutapi.entity.Friend;
import com.jy.friendcutapi.model.friend.FriendCreateRequest;
import com.jy.friendcutapi.model.friend.FriendIsCutChangeRequest;
import com.jy.friendcutapi.model.friend.FriendItem;
import com.jy.friendcutapi.repository.FriendRepository;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FriendService {
    private final FriendRepository friendRepository;

    public Friend getFriend(long id) {
        return friendRepository.findById(id).orElseThrow();
    }

    public void setFriend(FriendCreateRequest request) {
        Friend addData = new Friend();
        addData.setName(request.getName());
        addData.setBirthday(request.getBirthday());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setEtcMemo(request.getEtcMemo());
        addData.setIsCut(false);
        addData.setCutDay(request.getCutDay());
        addData.setWhyCut(request.getWhyCut());

        friendRepository.save(addData);
    }

    public List<FriendItem> getFriends() {
        List<Friend> originList = friendRepository.findAll();
        List<FriendItem> result = new LinkedList<>();

        for(Friend friend : originList) {
            FriendItem addItem = new FriendItem();
            addItem.setId(friend.getId());
            addItem.setName(friend.getName());
            addItem.setBirthday(friend.getBirthday());
            addItem.setEtcMemo(friend.getEtcMemo());
            addItem.setIsCut(friend.getIsCut());
            addItem.setCutDay(friend.getCutDay());
            addItem.setWhyCut(friend.getWhyCut());

            result.add(addItem);
        }

        return result;
    }

    public void putFriend(long id, FriendIsCutChangeRequest request) {
        Friend originData = friendRepository.findById(id).orElseThrow();
        originData.setIsCut(request.getIsCut());
        originData.setCutDay(request.getCutDay());
        originData.setWhyCut(request.getWhyCut());
        friendRepository.save(originData);
    }

    public void putFriendCutCancel(long id) {
        Friend originData = friendRepository.findById(id).orElseThrow();
        originData.setIsCut(false);
        originData.setCutDay(null);
        originData.setWhyCut(null);
        friendRepository.save(originData);
    }
}
