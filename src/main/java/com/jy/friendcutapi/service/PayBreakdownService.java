package com.jy.friendcutapi.service;

import com.jy.friendcutapi.entity.Friend;
import com.jy.friendcutapi.entity.PayBreakdown;
import com.jy.friendcutapi.model.payBreakdown.PayBreakdownCreateRequest;
import com.jy.friendcutapi.model.payBreakdown.PayBreakdownItem;
import com.jy.friendcutapi.model.payBreakdown.PayBreakdownWhatPayChangeRequest;
import com.jy.friendcutapi.repository.PayBreakdownRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PayBreakdownService {
    private final PayBreakdownRepository payBreakdownRepository;

    public void setPayBreakdown(Friend friend, PayBreakdownCreateRequest request) {
        PayBreakdown addData = new PayBreakdown();
        addData.setFriend(friend);
        addData.setDatePay(request.getDatePay());
        addData.setPayType(request.getPayType());
        addData.setWhatPay(request.getWhatPay());
        addData.setPrice(request.getPrice());

        payBreakdownRepository.save(addData);
    }

    public List<PayBreakdownItem> getPayBreakdowns() {
        List<PayBreakdown> originList = payBreakdownRepository.findAll();

        List<PayBreakdownItem> result = new LinkedList<>();

        for (PayBreakdown payBreakdown : originList ) {
            Friend friend = payBreakdown.getFriend();
            PayBreakdownItem addItem = new PayBreakdownItem();
            addItem.setMemberId(payBreakdown.getId());
            addItem.setMemberName(payBreakdown.getFriend().getName());
            addItem.setMemberPhoneNumber(payBreakdown.getFriend().getPhoneNumber());
            addItem.setDatePay(payBreakdown.getDatePay());
            addItem.setPayType(payBreakdown.getPayType());
            addItem.setWhatPay(payBreakdown.getWhatPay());
            addItem.setPrice(payBreakdown.getPrice());

            result.add(addItem);
        }

        return result;
    }

    public void putWhatPay(long id, PayBreakdownWhatPayChangeRequest request) {
        PayBreakdown originData = payBreakdownRepository.findById(id).orElseThrow();
        originData.setWhatPay(request.getWhatPay());
        payBreakdownRepository.save(originData);
    }

    public void delPayBreakdown(long id) {
        payBreakdownRepository.deleteById(id);
    }
}
