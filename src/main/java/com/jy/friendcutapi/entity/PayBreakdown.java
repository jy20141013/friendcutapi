package com.jy.friendcutapi.entity;

import com.jy.friendcutapi.enums.PayType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class PayBreakdown {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="friendId", nullable = false)
    private Friend friend;

    @Column(nullable = false)
    private LocalDate datePay;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private PayType payType;

    @Column(nullable = false)
    private String whatPay;

    @Column(nullable = false)
    private Integer price;
}
