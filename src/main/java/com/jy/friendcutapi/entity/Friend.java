package com.jy.friendcutapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Setter
@Getter
public class Friend {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false)
    private LocalDate birthday;

    @Column(nullable = false, length = 13)
    private String phoneNumber;

    @Column(nullable = true)
    private String etcMemo;

    @Column(nullable = false)
    private Boolean isCut;

    @Column(nullable = true)
    private LocalDate cutDay;

    @Column(nullable = true)
    private String whyCut;
}
