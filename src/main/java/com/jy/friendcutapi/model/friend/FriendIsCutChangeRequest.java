package com.jy.friendcutapi.model.friend;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class FriendIsCutChangeRequest {
    private Boolean isCut;
    private LocalDate cutDay;
    private String whyCut;
}
