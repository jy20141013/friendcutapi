package com.jy.friendcutapi.model.friend;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class FriendItem {
    private Long id;
    private String name;
    private LocalDate birthday;
    private String phoneNumber;
    private String etcMemo;
    private Boolean isCut;
    private LocalDate cutDay;
    private String whyCut;
}
