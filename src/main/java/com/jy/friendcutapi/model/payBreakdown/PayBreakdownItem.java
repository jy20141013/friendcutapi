package com.jy.friendcutapi.model.payBreakdown;

import com.jy.friendcutapi.enums.PayType;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class PayBreakdownItem {
    private Long memberId;
    private String memberName;
    private String memberPhoneNumber;
    private LocalDate datePay;
    private PayType payType;
    private String whatPay;
    private Integer price;
}
