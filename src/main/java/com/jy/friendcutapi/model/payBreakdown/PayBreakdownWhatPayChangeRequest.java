package com.jy.friendcutapi.model.payBreakdown;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PayBreakdownWhatPayChangeRequest {
    private String whatPay;
}
