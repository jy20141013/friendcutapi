package com.jy.friendcutapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FriendCutApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(FriendCutApiApplication.class, args);
	}

}
